package com.shbigdel.micros.calculation.add;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrosCalculationAddserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrosCalculationAddserviceApplication.class, args);
	}

}
