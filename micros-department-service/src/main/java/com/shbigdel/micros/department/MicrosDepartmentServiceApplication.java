package com.shbigdel.micros.department;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrosDepartmentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrosDepartmentServiceApplication.class, args);
	}

}
