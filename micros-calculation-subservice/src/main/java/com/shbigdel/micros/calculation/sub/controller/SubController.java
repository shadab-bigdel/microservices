package com.shbigdel.micros.calculation.sub.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/sub")
@Slf4j
public class SubController {

	@RequestMapping(value = "/{num1}/{num2}", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
	public String findDepartment(@PathVariable("num1") int num1, @PathVariable("num2") int num2) {
		log.info("subtraction numbers: " + num1 + " - "+ num2);
		return Integer.toString(num1 - num2);
	}

}
