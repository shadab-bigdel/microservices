package com.shbigdel.micros.calculation.sub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrosCalculationSubserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrosCalculationSubserviceApplication.class, args);
	}

}
